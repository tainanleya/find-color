﻿
// FindColorToolDlg.h: 头文件
//

#pragma once
#include "CDialog1.h"
#include "CDialog2.h"
#include <wchar.h>


// CFindColorToolDlg 对话框
class CFindColorToolDlg : public CDialogEx
{
// 构造
public:
	CFindColorToolDlg(CWnd* pParent = nullptr);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_FINDCOLORTOOL_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
private:
	CDialog1 m_cDialog1;
	CDialog2 m_cDialog2;
public:
	HDC m_targetDC;
};
