﻿
// FindColorTool.h: PROJECT_NAME 应用程序的主头文件
//

#pragma once

#ifndef __AFXWIN_H__
#error "在包含此文件之前包含 'pch.h' 以生成 PCH"
#endif

#include "resource.h"		// 主符号
#include "tool.h"
#include "mydll.h"
#include <string>

#define WM_MYMESSAGE (WM_USER+100)
#define WM_MYCUBEMESSAGE (WM_USER+101)

// CFindColorToolApp:
// 有关此类的实现，请参阅 FindColorTool.cpp
//

class CFindColorToolApp : public CWinApp
{
public:
	CFindColorToolApp();

	// 重写
public:
	virtual BOOL InitInstance();

	// 实现

	DECLARE_MESSAGE_MAP()
};

extern CFindColorToolApp theApp;
