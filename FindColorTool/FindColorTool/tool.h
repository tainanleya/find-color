#pragma once
#include <Windows.h>
#include <vector>
#include <iostream>

class Tool
{
public:
	static void DrawCube(HDC dc, const RECT& rect)
	{
		MoveToEx(dc, rect.left, rect.top, NULL);
		LineTo(dc, rect.left, rect.bottom);

		MoveToEx(dc, rect.left, rect.bottom, NULL);
		LineTo(dc, rect.right, rect.bottom);

		MoveToEx(dc, rect.right, rect.bottom, NULL);
		LineTo(dc, rect.right, rect.top);

		MoveToEx(dc, rect.right, rect.top, NULL);
		LineTo(dc, rect.left, rect.top);
	}
	static std::vector<std::string> mySplit(std::string str, char wc)
	{
		std::string temp;
		std::vector<std::string> res;
		for (char var : str)
		{
			if (var == wc)
			{
				if (!temp.empty())
					res.push_back(temp);
				temp.clear();
			}
			else
			{
				temp += var;
			}
		}
		res.push_back(temp);
		return res;
	}
	static std::vector<std::wstring> mySplit(std::wstring str, wchar_t wc)
	{
		std::wstring temp;
		std::vector<std::wstring> res;
		for (wchar_t var : str)
		{
			if (var == wc)
			{
				if (!temp.empty())
					res.push_back(temp);
				temp.clear();
			}
			else
			{
				temp += var;
			}
		}
		res.push_back(temp);
		return res;
	}
	static std::wstring char2wchar_t(std::string c)
	{
		std::string curLocale = setlocale(LC_ALL, NULL);   //curLocale="C"
		setlocale(LC_ALL, "chs");
		size_t num = 0;
		wchar_t* wc = new wchar_t[c.size() + 1];
		memset(wc, 0, c.size() + 1);
		mbstowcs_s(&num, wc, c.size() + 1, c.c_str(), _TRUNCATE);
		std::wstring result(wc);
		delete[] wc;
		setlocale(LC_ALL, curLocale.c_str());
		return result;
	}
	static std::string wchar_t2char(std::wstring w)
	{
		setlocale(LC_ALL, "chs");
		size_t num = 0;
		size_t c_size = w.size() * 2 + 1;
		char* ch = new char[c_size];
		memset(ch, 0, c_size);
		wcstombs_s(&num, ch, c_size, w.c_str(), _TRUNCATE);
		std::string result(ch);
		delete[] ch;
		setlocale(LC_ALL, "C");
		return result;
	}

	static std::string G2U(const char* gbk2312)
	{
		size_t n = MultiByteToWideChar(CP_ACP, 0, gbk2312, -1, NULL, 0);
		wchar_t* wc = new wchar_t[n + 1];
		MultiByteToWideChar(CP_ACP, 0, gbk2312, -1, wc, (int)n);

		n = WideCharToMultiByte(CP_UTF8, 0, wc, -1, NULL, 0, NULL, NULL);
		char* ch = new char[n + 1];
		WideCharToMultiByte(CP_UTF8, 0, wc, -1, ch, (int)n, NULL, NULL);
		std::string res(ch);
		delete[] ch;
		delete[] wc;
		return res;
	}
	static std::string U2G(const char* utf8)
	{
		size_t n = MultiByteToWideChar(CP_UTF8, 0, utf8, -1, NULL, 0);
		wchar_t* wc = new wchar_t[n + 1];
		MultiByteToWideChar(CP_UTF8, 0, utf8, -1, wc, n);
		n = WideCharToMultiByte(CP_ACP, 0, wc, -1, NULL, 0, NULL, NULL);
		char* ch = new char[n + 1];
		WideCharToMultiByte(CP_ACP, 0, wc, -1, ch, n, NULL, NULL);
		std::string res(ch);
		delete[]ch;
		delete[]wc;
		return res;
	}
};