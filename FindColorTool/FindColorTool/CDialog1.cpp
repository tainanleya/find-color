﻿// CDialog1.cpp: 实现文件
//

#include "pch.h"
#include "FindColorTool.h"
#include "CDialog1.h"
#include "CDialog2.h"
#include "afxdialogex.h"


// CDialog1 对话框

IMPLEMENT_DYNAMIC(CDialog1, CDialogEx)

CDialog1::CDialog1(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG1, pParent)
	, m_ClassName(_T(""))
	, m_Title(_T(""))
	, m_edit_point(_T(""))
{

}

CDialog1::~CDialog1()
{
}

void CDialog1::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_ClassName);
	DDX_Text(pDX, IDC_EDIT2, m_Title);
	DDX_Text(pDX, IDC_EDIT17, m_edit_point);
}


BEGIN_MESSAGE_MAP(CDialog1, CDialogEx)
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_CTLCOLOR()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_MESSAGE(WM_MYMESSAGE, OnMyMessage)
	ON_MESSAGE(WM_MYCUBEMESSAGE, OnMyCubeMessage)
	ON_BN_CLICKED(IDC_BUTTON1, &CDialog1::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CDialog1::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CDialog1::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CDialog1::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON5, &CDialog1::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON6, &CDialog1::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON7, &CDialog1::OnBnClickedButton7)
	ON_BN_CLICKED(IDC_BUTTON8, &CDialog1::OnBnClickedButton8)
	ON_BN_CLICKED(IDC_BUTTON9, &CDialog1::OnBnClickedButton9)
	ON_BN_CLICKED(IDC_BUTTON10, &CDialog1::OnBnClickedButton10)
	ON_BN_CLICKED(IDC_BUTTON11, &CDialog1::OnBnClickedButton11)
	ON_BN_CLICKED(IDC_BUTTON12, &CDialog1::OnBnClickedButton12)
	ON_BN_CLICKED(IDC_BUTTON13, &CDialog1::OnBnClickedButton13)
	ON_BN_CLICKED(IDC_BUTTON14, &CDialog1::OnBnClickedButton14)
	ON_BN_CLICKED(IDC_BUTTON15, &CDialog1::OnBnClickedButton15)
	ON_BN_CLICKED(IDC_BUTTON16, &CDialog1::OnBnClickedButton16)
	ON_BN_CLICKED(IDC_BUTTON17, &CDialog1::OnBnClickedButton17)
END_MESSAGE_MAP()


// CDialog1 消息处理程序


void CDialog1::OnPaint()
{
	CPaintDC dc(this); // device context for painting
					   // TODO: 在此处添加消息处理程序代码
					   // 不为绘图消息调用 CDialogEx::OnPaint()

	DrawColor(dc, m_rangeRGB);

}


int CDialog1::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  在此添加您专用的创建代码
	RGBTRIPLE tempRGB = { 0,0,255 };
	for (size_t i = 0; i < MAX_COLOR; i++)
	{
		m_rangeRGB.push_back(std::vector<RGBTRIPLE>());
		for (size_t j = 0; j < MAX_COLOR; j++)
		{
			m_rangeRGB[i].push_back(tempRGB);
		}
	}
	return 0;
}

void CDialog1::DrawColor(CPaintDC& dc, const std::vector<std::vector<RGBTRIPLE>>& rgb)
{
	int interval = PIXELSIZE, boxLong = rgb.size();
	int x1 = 20, y1 = 100;
	HPEN hPen;
	hPen = CreatePen(PS_SOLID, 0, RGB(50, 50, 50));
	RECT rect;
	GetWindowRect(&rect);
	HDC hdc = CreateCompatibleDC(dc);
	HBITMAP temp = (HBITMAP)LoadImage(NULL, L"images/bg.bmp", IMAGE_BITMAP,
		rect.right - rect.left, rect.bottom - rect.top, LR_LOADFROMFILE);
	SelectObject(hdc, temp);
	SelectObject(hdc, hPen);

	//x轴 竖的
	for (int i = 0; i <= boxLong; i++)
	{
		MoveToEx(hdc, x1 + i * interval, y1, 0);
		LineTo(hdc, x1 + i * interval, y1 + boxLong * interval);
	}
	//y轴 横的
	for (int i = 0; i <= boxLong; i++)
	{
		MoveToEx(hdc, x1, y1 + i * interval, 0);
		LineTo(hdc, x1 + interval * boxLong, y1 + i * interval);
	}

	int Xnumber = 0, Ynumber = 0;
	HBRUSH brush;
	RECT rect1;
	std::for_each(rgb.begin(), rgb.end(), [&](std::vector<RGBTRIPLE> m_rgb)
		{
			std::for_each(m_rgb.begin(), m_rgb.end(), [&](RGBTRIPLE rgbTriple)
				{

					brush = CreateSolidBrush(RGB(rgbTriple.rgbtRed, rgbTriple.rgbtGreen, rgbTriple.rgbtBlue));
					rect1 = {
						x1 + (interval * Xnumber) + 1, y1 + (interval * Ynumber) + 1,
						x1 + (interval * Xnumber) + interval , y1 + (interval * Ynumber) + interval,
					};
					FillRect(hdc, &rect1, brush);
					DeleteObject(brush);
					Xnumber++;
				});
			Ynumber++;
			Xnumber = 0;
		});

	//在中间画个红色的框
	DeleteObject(hPen);
	hPen = CreatePen(PS_SOLID, 3, RGB(0, 0, 255));
	SelectObject(hdc, hPen);

	int core = boxLong / 2;
	Tool::DrawCube(hdc, {
		x1 + interval * core ,
		 y1 + interval * core ,
		x1 + interval * core + interval ,
		 y1 + interval * core + interval });

	BitBlt(dc, 0, 0, rect.right - rect.left, rect.bottom - rect.top, hdc, 0, 0, SRCCOPY);

	DeleteObject(hPen);
	DeleteObject(hdc);
	DeleteObject(temp);
}

HBRUSH CDialog1::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);
	if (nCtlColor == CTLCOLOR_STATIC)
	{
		pDC->SetTextColor(RGB(0, 255, 100));
		pDC->SetBkMode(TRANSPARENT);
		return (HBRUSH)GetStockObject(NULL_BRUSH);
	}
	return hbr;
}

LRESULT CDialog1::OnMyMessage(WPARAM wParam, LPARAM lParam)
{
	CDialog2* dialog2 = (CDialog2*)lParam;
	m_rangeRGB = dialog2->tempRGB;
	int Xnumber = 0, Ynumber = 0;
	HBRUSH brush;
	RECT rect1;
	int interval = PIXELSIZE, boxLong = m_rangeRGB.size();
	int x1 = 20, y1 = 100;
	HDC temp = ::GetDC(m_hWnd);
	HDC hdc = CreateCompatibleDC(temp);
	GetWindowRect(&rect1);

	HBITMAP bitmap = CreateCompatibleBitmap(temp, rect1.right - rect1.left, rect1.bottom - rect1.top);
	SelectObject(hdc, bitmap);

	std::for_each(m_rangeRGB.begin(), m_rangeRGB.end(), [&](std::vector<RGBTRIPLE> m_rgb)
		{
			std::for_each(m_rgb.begin(), m_rgb.end(), [&](RGBTRIPLE rgbTriple)
				{
					brush = CreateSolidBrush(RGB(rgbTriple.rgbtRed, rgbTriple.rgbtGreen, rgbTriple.rgbtBlue));
					rect1 = {
						x1 + (interval * Xnumber) + 1, y1 + (interval * Ynumber) + 1,
						x1 + (interval * Xnumber) + interval , y1 + (interval * Ynumber) + interval,
					};
					FillRect(hdc, &rect1, brush);
					DeleteObject(brush);
					Xnumber++;
				});
			Ynumber++;
			Xnumber = 0;
		});

	HPEN hPen;
	hPen = CreatePen(PS_SOLID, 3, RGB(0, 0, 255));
	SelectObject(hdc, hPen);

	int core = boxLong / 2;
	Tool::DrawCube(hdc, {
		x1 + interval * core ,
		 y1 + interval * core ,
		x1 + interval * core + interval ,
		 y1 + interval * core + interval });

	GetWindowRect(&rect1);
	//BitBlt(temp, 100, 20, rect1.right - rect1.left, rect1.bottom - rect1.top, hdc, 0, 0, SRCCOPY);
	BitBlt(temp, x1, y1, interval * boxLong, interval * boxLong, hdc, x1, y1, SRCCOPY);

	wchar_t wch[255];
	wsprintf(wch, L"(%d,%d)", dialog2->m_curMousePt.x, dialog2->m_curMousePt.y);
	GetDlgItem(IDC_EDIT17)->SetWindowTextW(wch);
	int i_temp = m_rangeRGB.size() / 2;

	wsprintf(wch, L"%d", m_rangeRGB[i_temp][i_temp].rgbtRed);
	GetDlgItem(IDC_EDIT18)->SetWindowTextW(wch);

	wsprintf(wch, L"%d", m_rangeRGB[i_temp][i_temp].rgbtGreen);
	GetDlgItem(IDC_EDIT19)->SetWindowTextW(wch);

	wsprintf(wch, L"%d", m_rangeRGB[i_temp][i_temp].rgbtBlue);
	GetDlgItem(IDC_EDIT20)->SetWindowTextW(wch);

	DeleteDC(hdc);
	DeleteDC(temp);
	DeleteObject(hPen);
	DeleteObject(bitmap);
	return LRESULT();
}


void CDialog1::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	HCURSOR hcursor = AfxGetApp()->LoadCursorW(IDC_CURSOR2);
	SetCursor(hcursor);
	SetCapture();
	CDialogEx::OnLButtonDown(nFlags, point);
}

void CDialog1::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	POINT pt;
	GetCursorPos(&pt);
	g_targetHwnd = ::WindowFromPoint(pt);
	WCHAR ch[255];
	::GetWindowText(g_targetHwnd, ch, 255);
	m_Title.SetString(ch);
	ZeroMemory(ch, 255);
	::GetClassName(g_targetHwnd, ch, 255);
	m_ClassName.SetString(ch);

	RECT rect;
	::GetWindowRect(g_targetHwnd, &rect);
	wsprintf(ch, L"%d,%d,%d,%d", 0, 0, rect.right - rect.left, rect.bottom - rect.top);
	GetDlgItem(IDC_EDIT21)->SetWindowTextW(ch);

	UpdateData(FALSE);
	ReleaseCapture();
	CDialogEx::OnLButtonUp(nFlags, point);
}

LRESULT CDialog1::OnMyCubeMessage(WPARAM wParam, LPARAM lParam)
{
	POINT pt;
	RECT rect;
	GetCursorPos(&pt);
	CDialog2* dialog2 = (CDialog2*)lParam;
	WCHAR wch[255];
	::GetWindowRect(dialog2->m_hWnd, &rect);
	wsprintf(wch, L"%d,%d,%d,%d", dialog2->m_cubeMousePt.x, dialog2->m_cubeMousePt.y, pt.x - rect.left, pt.y - rect.top);
	GetDlgItem(IDC_EDIT21)->SetWindowTextW(wch);

	HDC dc = ::GetDC(dialog2->m_hWnd);
	BitBlt(dc, 0, 0, rect.right - rect.left, rect.bottom - rect.top, dialog2->m_targerDC, 0, 0, SRCCOPY);
	DeleteDC(dc);

	return 0;
}


void CDialog1::OnBnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码
	int ID = 1003;
	for (size_t i = 1; i <= 14; i++)
		GetDlgItem(ID + i)->SetWindowTextW(L"");
}


void CDialog1::OnBnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码
	GetDlgItem(IDC_EDIT3)->SetWindowTextW(L"");
}


void CDialog1::OnBnClickedButton3()
{
	// TODO: 在此添加控件通知处理程序代码
	GetDlgItem(IDC_EDIT4)->SetWindowTextW(L"");
}


void CDialog1::OnBnClickedButton4()
{
	// TODO: 在此添加控件通知处理程序代码
	GetDlgItem(IDC_EDIT5)->SetWindowTextW(L"");
}


void CDialog1::OnBnClickedButton5()
{
	// TODO: 在此添加控件通知处理程序代码
	GetDlgItem(IDC_EDIT6)->SetWindowTextW(L"");
}


void CDialog1::OnBnClickedButton6()
{
	GetDlgItem(IDC_EDIT7)->SetWindowTextW(L"");
	// TODO: 在此添加控件通知处理程序代码
}


void CDialog1::OnBnClickedButton7()
{
	// TODO: 在此添加控件通知处理程序代码
	GetDlgItem(IDC_EDIT8)->SetWindowTextW(L"");
}


void CDialog1::OnBnClickedButton8()
{
	// TODO: 在此添加控件通知处理程序代码
	GetDlgItem(IDC_EDIT9)->SetWindowTextW(L"");
}


void CDialog1::OnBnClickedButton9()
{
	// TODO: 在此添加控件通知处理程序代码
	GetDlgItem(IDC_EDIT10)->SetWindowTextW(L"");
}


void CDialog1::OnBnClickedButton10()
{
	// TODO: 在此添加控件通知处理程序代码
	GetDlgItem(IDC_EDIT11)->SetWindowTextW(L"");
}


void CDialog1::OnBnClickedButton11()
{
	// TODO: 在此添加控件通知处理程序代码
	GetDlgItem(IDC_EDIT12)->SetWindowTextW(L"");
}


void CDialog1::OnBnClickedButton12()
{
	// TODO: 在此添加控件通知处理程序代码
	GetDlgItem(IDC_EDIT13)->SetWindowTextW(L"");
}


void CDialog1::OnBnClickedButton13()
{
	// TODO: 在此添加控件通知处理程序代码
	GetDlgItem(IDC_EDIT14)->SetWindowTextW(L"");
}


void CDialog1::OnBnClickedButton14()
{
	// TODO: 在此添加控件通知处理程序代码
	GetDlgItem(IDC_EDIT15)->SetWindowTextW(L"");
}


void CDialog1::OnBnClickedButton15()
{
	// TODO: 在此添加控件通知处理程序代码
	GetDlgItem(IDC_EDIT16)->SetWindowTextW(L"");
}


void CDialog1::OnBnClickedButton16()
{
	// TODO: 在此添加控件通知处理程序代码
	CString cwch, target, Ctemp;
	std::vector<wstring> v_str;
	WCHAR temp[256];
	POINT pt;
	int number = 0, x1, y1;
	// ({x1,y1,x2,y2},"colordata",)
	/*
	MatchingGrating
	(const RECT& rect, const char* rgbcolor, POINT& point, const int& deviation = 13, const int& RanPixel = 1);
	({x1,y1,x2,y2},"colordata",point)
	*/
	GetDlgItemTextW(IDC_EDIT21, cwch);
	target += L"POINT point;\t\t\tMatchingGrating";
	wsprintf(temp, L"({%s},\"", cwch.GetBuffer());
	target += temp;

	for (size_t i = 1; i <= 14; i++)
	{
		cwch.Empty();
		GetDlgItemTextW(1003 + i, cwch);
		if (cwch != L"")
		{
			if (i != 1)
				target += L",";
			v_str = Tool::mySplit(cwch.GetString(), L',');
			if (v_str.size() == 3)
			{
				x1 = atoi(Tool::wchar_t2char(v_str[0].c_str()).c_str());
				y1 = atoi(Tool::wchar_t2char(v_str[1].c_str()).c_str());
				if (number == 0)
				{
					pt.x = x1;
					pt.y = y1;
				}
				if (pt.x > x1)
				{
					_itow_s(pt.x - x1, temp, 10);
					target += L"-";
					target += temp;
					target += L"|";
				}
				else
				{
					_itow_s(x1 - pt.x, temp, 10);
					target += temp;
					target += L"|";
				}

				if (pt.y > y1)
				{
					_itow_s(pt.y - y1, temp, 10);
					target += L"-";
					target += temp;
					target += L"|";
				}
				else
				{
					_itow_s(y1 - pt.y, temp, 10);
					target += temp;
					target += L"|";
				}

				target += v_str[2].c_str();
			}
			number++;
		}
	}
	target += L"\",point,13,1);\r";
	target += L"\tif(point.x!=-1){/*TODO*/};";
	GetDlgItem(IDC_EDIT22)->SetWindowTextW(target);
}


void CDialog1::OnBnClickedButton17()
{
	// TODO: 在此添加控件通知处理程序代码
	myDIB dib;
	POINT point;
	RECT rect;
	dib.BindHwnd(g_targetHwnd);
	::GetWindowRect(g_targetHwnd, &rect);
	dib.CaptureRange(0, 0, rect.right - rect.left, rect.bottom - rect.top);

	CString Cstr;
	GetDlgItemText(IDC_EDIT22, Cstr);
	std::string res = Tool::wchar_t2char(Cstr.GetString());

	if (res.find('\"') == -1 || res.find('}') == -1)
		return;
	int n = 0;
	while ((n = res.find('\r')) != -1)
		res.erase(n, 1);
	while ((n = res.find('\n')) != -1)
		res.erase(n, 1);
	while ((n = res.find(' ')) != -1)
		res.erase(n, 1);
	while ((n = res.find('\t')) != -1)
		res.erase(n, 1);

	std::string color = res.substr(res.find('"') + 1, res.find_last_of('"') - res.find('"') - 1);
	std::string rectStr = res.substr(res.find('{') + 1, res.find('}') - res.find('{') - 1);

	std::vector<std::string> v_rectStr = Tool::mySplit(rectStr, ',');
	if (v_rectStr.size() != 4)
		return;

	rect = {
		atol(v_rectStr[0].c_str()),
		atol(v_rectStr[1].c_str()),
		atol(v_rectStr[2].c_str()),
		atol(v_rectStr[3].c_str()) };

	char ch[1024];
	ZeroMemory(ch, 1024);
	for (size_t i = 0; i < color.size(); i++)
	{
		ch[i] = color[i];
	}


	mytime time;


	time.Begin();
	dib.MatchingGrating(rect, ch, point, 13, 1);
	time.End();
	if (point.x != -1)
	{
		//TODO
		char w_ch[256];
		sprintf_s(w_ch, "成功找到点位置 (%d,%d)  耗时:%.4lf", point.x, point.y, time.Interval);
		MessageBoxW(Tool::char2wchar_t(w_ch).c_str(), L"工具");
	};
}
