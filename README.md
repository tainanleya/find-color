# findColor

#### 介绍
多点找色,使用该dll可以多点找色.


#### 安装教程

DLL:可以使用vs工具打开Dll.sln文件,直接编译成动态dll,也有编译好的64位动态dll
FindColorTool:可以使用vs工具直接打开FindColorTool.sln文件,直接编译,也有编译好之后的exe执行程序


#### 使用说明

多点找色DLL使用说明:

1.导入dll到项目中,并添加上头文件

2.创建一个myDIB类对象

3.使用函数BindHwnd(HWND hwnd),绑定需要扫描的句柄.

4.使用函数CaptureRange(WORD x, WORD y, WORD w, WORD h),绑定一个扫描的范围

5.使用MatchingGrating函数找点位即可

BOOL MatchingGrating(const RECT& rect, const char* rgbcolor, POINT& point, const int& deviation = 13, const int& RanPixel = 1)

API详解:该函数会扫描指定的范围内有没有符合要求的多点色块,有则返回true,无则返回false.

rect:指定扫描的范围,基于上一个函数的范围之中计算.

rgbcolor:一串多点色图的字符串,可以使用FindColorTool工具来获取.

point:如果寻找到了符合的点位,将点位信息赋值到point,未找到,point置为-1.

deviation:RBG像素的允许的差值,在这个差值里面都算符合.

RanPixel:查找某点位可以的范围,例如2查找周围1圈,3查找周围2圈.

---

鼠标点击API:x,y坐标,delay是按下和弹起直接的间隔

BOOL MYDLL_API LMouseClick(WORD x, WORD y, int delay = 10);

BOOL MYDLL_API RMouseClick(WORD x, WORD y, int delay = 10);

---

FindColorTool工具:

![工具演示](%E6%BC%94%E7%A4%BA%E8%A7%86%E9%A2%91.gif)

使用小键盘1-9可以设置颜色1-9 0设置10

上下左右按键可以一个像素移动鼠标

alt+a 开始选取范围 als+s 结束选取范围
